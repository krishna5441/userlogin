<?php

namespace App\Http\Controllers;

class ListController extends Controller
{
    public function show()
    {
        $characters = [
            'Monday'    => '10',
            'Tuesday'   => '11',
            'Wednesday' => '12',
            'Thursday'  => '13',
            'Friday'    => '14',
            'Saturday'  => '15',
            'Sunday'    => '16',

        ];

        return view('welcome')->withCharacters($characters);
    }
}
