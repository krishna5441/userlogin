<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/',function (){


    $user=Auth::user();
    if($user->isAdmin()){

        return view('recruiter');


    }
   return view('employee');

});



Route::get('/listcontroller', 'ListController@show');
Route::get('/listcontroller', 'ListController@show');
Route::auth();

Route::get('/home', 'HomeController@index');

Route::controllers([
    'password' => 'Auth\PasswordController',
]);

Route::get('/admin/user/roles',['middleware'=>['role','auth','web'],function (){


    return "middleware role";
}]);